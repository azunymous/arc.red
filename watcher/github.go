package watcher

import (
	"fmt"
	"github.com/google/go-github/v39/github"
)

const Owner = "azunymous"
const Repo = "arc"

const automatedLabel = "automated"
const eventLabel = "event"

type IssueWebhook struct {
	Action string `json:"action"`
	Issue  github.Issue
}

func IsValid(i *github.Issue) error {
	c := 0
	var labels []string
	for _, label := range i.Labels {
		labels = append(labels, *label.Name)
		if *label.Name == automatedLabel || *label.Name == eventLabel {
			c++
		}
	}
	if c == 2 {
		return nil
	}
	return fmt.Errorf("expected %s and %s labels, got: %v", automatedLabel, eventLabel, labels)
}
