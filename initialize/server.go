package initialize

import (
	"github.com/azunymous/arc/config"
	"github.com/azunymous/arc/server"
	"github.com/azunymous/arc/tournament"
	"log"
)

func Server() (*server.Srv, error) {
	tournaments, err := tournament.From(config.Tournaments())
	if err != nil {
		log.Fatalf("Could not parse tournaments: %v", err)
	}
	t := tournament.NewTournamentsFromConfig(tournaments)
	srv := server.NewSrv(t)
	return srv, err
}
