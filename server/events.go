package server

import (
	"fmt"
	"github.com/azunymous/arc/tournament"
	"github.com/azunymous/arc/watcher"
	"github.com/gofiber/fiber/v2"
	"github.com/google/go-github/v39/github"
	"log"
)

type Srv struct {
	t        *tournament.Tournaments
	ghClient *github.Client
}

func NewSrv(t *tournament.Tournaments) *Srv {
	client := github.NewClient(nil)
	return &Srv{t: t, ghClient: client}
}

func (s *Srv) Events(ctx *fiber.Ctx) error {
	return ctx.JSON(&s.t)
}

func (s *Srv) GHWatcher(ctx *fiber.Ctx) error {
	hmac := ctx.Get("X-Hub-Signature-256")
	if hmac == "" {
		return fmt.Errorf("expected HMAC header, got nothing")
	}
	// TODO actually validate HMAC

	var webhook watcher.IssueWebhook
	err := ctx.BodyParser(&webhook)

	if err != nil {
		e := fmt.Sprintf("Failed to read body: %v", err)
		log.Println(e)
		return ctx.SendString(e)
	}

	issue, _, err := s.ghClient.Issues.Get(ctx.Context(), watcher.Owner, watcher.Repo, *webhook.Issue.Number)
	if err != nil {
		return err
	}
	if err := watcher.IsValid(issue); err != nil {
		return err
	}

	fmt.Println(*issue.Body)
	// TODO actually write the issue / update the issue in the config
	return nil
}
